from asyncore import read
from fileinput import close

from numpy import true_divide
from jurassic_park import dinosaurs
#from lista import Lista
from lista import ordenamiento
from lista import barrido
from cola import Cola
#time;zone_code;dino_number;alert_level
class Dinosaurio:
     def __init__(self, time, zone_code, dino_number, alert_level, name, tipo, periodo):
          self.time = time
          self.zone_code = zone_code
          self.dino_number = dino_number
          self.alert_level = alert_level
          self.name = name
          self.tipo = tipo
          self.periodo = periodo
     def __str__(self) -> str:
         return f'{self.time} {self.zone_code} {self.dino_number} {self.alert_level} {self.name} {self.tipo} {self.periodo}'
          
ordenamiento_zona = lambda dino: dino.zone_code
ordenamiento_fecha = lambda dino: dino.time
ordenamiento_tipo = lambda dino: dino.tipo
ordenamiento_periodo = lambda dino: dino.periodo
lista_jurassic = []
lista_jurassic2 = []

def buscar_dino_por_num(lista_dinos, num):
     
    for i, dino in enumerate(lista_dinos):
          if str(dino.dino_number) == str(num):
               return i
          else:
               return None
def buscar_dino_por_nombre(lista_dinos, nombre):
     
     for dino in lista_dinos:
          if str(dino.name) == nombre:
               print(dino)
               
def buscar_dino_por_nombre2(lista_dinos, nombre):
     '''devuelve una lista de los dinos encontrados'''
     lista_i=[]
     for i, dino in enumerate(lista_dinos):
          if str(dino.name) == nombre:
               print(dino)
               lista_i.append(dino)
     
def buscar_dino_por_zona(lista_dinos, zona):
     '''devuelve una lista de los indices donde encontrar los dinos que estén en dichas zonas'''
     centinela = False
     lista_i_result=[]
     for i, dino in enumerate(lista_dinos):
          if str(dino.zone_code) == str(zona):
               centinela = True
               lista_i_result.append(i)
               print(dino)
     return lista_i_result

def buscar_dino_por_tipo(lista_dinos, tipo):
     centinela = False
     for i, dino in enumerate(lista_dinos):
          if str(dino.tipo) == str(tipo):
               centinela = True
               return i
          
     if centinela == False:
          return None
def eliminar_dino_por_zona(lista_dinos, zona):
     for i, dino in enumerate(lista_dinos):
          if str(dino.zone_code) == str(zona):
               lista_dinos.pop(i)
     
#file = open('D:\Usuario Israel\Documents\OneDrive - ConcepcióndelUruguay\UADER\lic sistemas 2do año\\2do\\algoritmos y estrucutras de datos\parcial-algoritmos-junio-2022\alerts.txt', mode='r', encoding='UTF-8')
#file = open('alerts.txt', mode='r')
#file = open('/home/tanuki/Documentos/uader/2do/algoritmos y estrucutras de datos/phyton/parcial-algoritmos-junio-2022/alerts.txt', mode='r')
file = open('D:/Usuario Israel/Documents/OneDrive - ConcepcióndelUruguay/UADER/lic sistemas 2do año/2do/algoritmos y estrucutras de datos/parcial-algoritmos-junio-2022/alerts.txt', mode='r', encoding='UTF-8')

lineas = file.readlines()

def busqueda_dinosaurio(num_buscado):
     for dinosaurio_dict in dinosaurs:
          if dinosaurio_dict["number"] == num_buscado:
              
               nombreDino = dinosaurio_dict["name"]
               tipoDino = dinosaurio_dict["type"]
               periodoDino = dinosaurio_dict["period"]
               return nombreDino, tipoDino, periodoDino


lista = []

lineas.pop(0)  # quitar cabecera
for linea in lineas:
     dato = linea.split(';')
     
     #print(dato)
     nombredino, tipodino, periododino = busqueda_dinosaurio(int(dato[2]))  
     dino = Dinosaurio(dato[0], dato[1], dato[2], dato[3], nombredino, tipodino, periododino)

     lista_jurassic.append(dino)

     lista_jurassic2.append(dino)  
lista_jurassic.sort(key=ordenamiento_zona)
lista_jurassic2.sort(key=ordenamiento_fecha)

file.close()


print('------------------')




def ultimo_descubierto():
     mayor = 0
     buscado = None
     for dinosaurio_dict in dinosaurs:
          nombrado = dinosaurio_dict['named_by']
          nombrado_list = nombrado.split(',')
          item = int(nombrado_list[1].strip())
          
          if (item > mayor):
               mayor = item
               buscado = dinosaurio_dict
     return buscado



#WYG075, SXH966 y LYF010

eliminar_dino_por_zona(lista_jurassic, "WYG075")
eliminar_dino_por_zona(lista_jurassic, "SXH966")
eliminar_dino_por_zona(lista_jurassic, "LYF010")

eliminar_dino_por_zona(lista_jurassic2, "WYG075")
eliminar_dino_por_zona(lista_jurassic2, "SXH966")
eliminar_dino_por_zona(lista_jurassic2, "LYF010")

#lem = lista_jurassic.index(buscar_dino_por_zona(lista_jurassic, "HYD195"))
#(buscar_dino_por_zona(lista_jurassic, "HYD195"))
zona_HYD195 = buscar_dino_por_zona(lista_jurassic, "HYD195")
lista_jurassic[zona_HYD195[0]].name="Mosasaurus"
#lista_jurassic[buscar_dino_por_zona(lista_jurassic2, "HYD195")].name="Mosasaurus"
#barrido(lista_jurassic)

#Pero que está pasando llevo un tiempo intentando comunicarme, las alarmas de
# incidentes están como locas, no sé a qué lugar debo ir primero con mi equipo de contención. Necesito
# que tomes toda la información de alertas, y las insertes en dos colas, una con los datos de dinosaurios
# carnívoros y otra con los herbívoros, descarten las de nivel ‘low’ y ‘medium’. [actividad

cola_herviboro = Cola()
cola_carnivoro = Cola()

def encolar_herv_Y_carn(lista_dino, cola_herviboro, cola_carnivoro):
          
     for dino in lista_dino:
          if (dino.tipo == "herbívoro ") and("high" in dino.alert_level ):
               cola_herviboro.arribo(dino)
          elif (dino.tipo == "carnívoro ")and("high" in dino.alert_level ):
               cola_carnivoro.arribo(dino)

# Atención centro de monitoro no podemos acceder al sistema de colas de
# alertas por un aparente problema de conexión, atiendan las alertas en la cola de carnívoros y muestren
# en la pantalla (para que se publiquen en el canal de alerta de banda segura) la información pero
# descarten las provenientes de la zona EPC944, ya se encuentra una unidad de respaldo ahí.


def encolar_descarta_zona(cola_carnivoro):
     for i in range(cola_carnivoro.tamanio()):
          if (dino.zone_code == "EPC944"):
              cola_carnivoro.atencion()


# Oigan perdón que los molestes, pero no consigo que el sistema me funcione en mi
# notebook, podrían pasarme un listado de toda la información que tienen procesada del archivo, pero
# solo de los dinosaurios Raptors y Carnotaurus; y los códigos de las zonas donde puedo encontrar
# dinosaurios Compsognathus. Que sea lo antes posible hoy es un día muy agitado.

# si el número está entre 33 y 47 su valor alfanumérico esta ok.
# 2. caso contrario
#    si número es divisible por 3 entonces (número // 2) + 9 (es tu nuevo valor alfanumérico)
#    sino número -14 (es tu nuevo valor alfanumérico)
# en cualquiera de los casos debes continuar procesandolo, es una solución parcial.
# 3. al final obtendrás la clave si sabes cómo hacer las cosas, pero recuerda ‘mosquito’ es la clave
# de todo.

def adivinar_clave(frase, num, clave, i):
     
     i=0
     num=ord(frase[i])
     clave.append('')
     if (num >= 33) and (num <=47):
          clave.append(chr(num))
     else:
          if num % 3 == 0:
               
               num = (num//2)+9
          else:
               num= num-14
     #print(num,clave[i], i)
     if i <= len(frase):
          i=i+1
     else: 
          return None
     adivinar_clave(frase, num, clave, i)









def menu():
     salir = False
     while salir == False:
          print('ingrese opcion')
          print('0. salir')
          print('1. mostrar listado por zona')
          print('2. mostrar listado por hora')
          print('3. mostrar ultimo descubierto')
          print('4. filtrar resultados Tyrannosaurus Rex, Spinosaurus, Giganotosaurus con nivel  ́critical’ o ‘high’.')
          print('5. encolar carnivors y hervíboros que no sean ni critical ni low ni medium')
          # print('6. mostrar información de dinosaurios en concreto')
          # print('7. mostrar codigo de zona de dinosaurio')
          print('8. adivinar contraseña')
          print('9. descartar zona EPC944 de la cola de carnivoros')
          print('10. mostrar cola ')
          print('11. mostrar listado dinosaurio  Raptors y Carnotaurus')
          tecla=int(input())
          if tecla == 1:
               barrido(lista_jurassic)
          elif tecla == 2:
               barrido(lista_jurassic2)
          elif tecla == 3:
               print("el ultimo dinosaurio descubierto fue: ", ultimo_descubierto())
          elif tecla == 4:
# Necesito urgente un listado filtrado de los
# datos que solo incluya datos de los dinosaurios: Tyrannosaurus Rex, Spinosaurus, Giganotosaurus con
# nivel  ́critical’ o ‘high’.
               Tyrannosaurus_Rex=buscar_dino_por_nombre2(lista_jurassic,"Tyrannosaurus Rex")
               Spinosaurus=buscar_dino_por_nombre2(lista_jurassic,"Spinosaurus")
               Giganotosaurus=buscar_dino_por_nombre2(lista_jurassic,"Giganotosaurus")
               for dino in Tyrannosaurus_Rex:
                    if not ("low" in Tyrannosaurus_Rex.alert_level):
                         print(dino)
               for dino in Spinosaurus:
                    if not ("low" in Spinosaurus.alert_level):
                         print(dino)
               for dino in Giganotosaurus:
                    if not ("low" in Giganotosaurus.alert_level):
                         print(dino)
                    
                    


          elif tecla == 5:
               encolar_herv_Y_carn(lista_jurassic, cola_herviboro, cola_carnivoro)

          # elif tecla == 6:
          
          # elif tecla == 7:
          elif tecla == 8:
               clave=[]
               num=int
               i=0
               palabra=input("ingrese la clave: ")
               adivinar_clave(palabra, num, clave, i)
               print("la clave es: ", clave)
          elif tecla == 9:
               encolar_descarta_zona(cola_carnivoro)
          elif tecla == 10:     
               print("cantidad de herviboros nivel high", cola_herviboro.tamanio())
               print("cantidad de carnivoros nivel high", cola_carnivoro.tamanio())
               opcion=int(input(" presione 1 para mostrar cola herviboros y 2 para carnivoros"))
               if opcion == 1:
                    for i in range(cola_herviboro.tamanio()):
                         print(cola_herviboro.atencion())
               if opcion == 2:
                    for i in range(cola_carnivoro.tamanio()):
                         print(cola_carnivoro.atencion())
          elif tecla == 11:
               buscar_dino_por_nombre(lista_jurassic, 'Raptors')
               buscar_dino_por_nombre(lista_jurassic, 'Carnotaurus')
               print('--------- codigos de zona dinosaurio Compsognathus --------- ')
               for dino in lista_jurassic:
                    if dino.name == 'Compsognathus':
                         print(dino.zone_code)
               buscar_dino_por_nombre(lista_jurassic, 'Compsognathus')
          elif tecla == 0:
               salir = True

menu()

